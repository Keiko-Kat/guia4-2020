#include "Nodo.h"
#include "Arbol.h"
using namespace std;
#include <string>
#include <iostream>
#include <fstream>

class Ejercicio {
    private:
        Arbol *arbol = NULL;

    public:
        /* constructor */
        Ejercicio() {
            this->arbol = new Arbol();
        }
        
        Arbol *get_arbol() {
            return this->arbol;
        }
        
		bool validar_int(string in){
			/*para validar la entrada de un int*/
			bool is_true = true;
			
			for(int j = 0; j < in.size(); j++){
				/* se comparan los valores ASCII*/
				if(in[j] < 48 || in[j] >57){
					is_true = false;
				}
			}
			return is_true;
		}
		
        int menu(){
			string in;
			bool is_true;
			int temp, cond = 0;
			while(cond == 0){
				cout<<"Agregar otro número [1]"<<endl;
				cout<<"~~~~~~~~~~~~~~~~~~~~~~~"<<endl;
				cout<<"Eliminar número     [2]"<<endl;
				cout<<"~~~~~~~~~~~~~~~~~~~~~~~"<<endl;
				cout<<"Modificar número    [3]"<<endl;
				cout<<"~~~~~~~~~~~~~~~~~~~~~~~"<<endl;
				cout<<"Leer preorden:      [4]"<<endl;
				cout<<"~~~~~~~~~~~~~~~~~~~~~~~"<<endl;
				cout<<"Leer inorden:       [5]"<<endl;
				cout<<"~~~~~~~~~~~~~~~~~~~~~~~"<<endl;
				cout<<"Leer posorden:      [6]"<<endl;
				cout<<"~~~~~~~~~~~~~~~~~~~~~~~"<<endl;
				cout<<"Generar grafo:      [7]"<<endl;
				cout<<"~~~~~~~~~~~~~~~~~~~~~~~"<<endl;
				cout<<"Salir               [0]"<<endl;
				cout<<"-----------------------"<<endl;
				cout<<"-----------------------"<<endl;
				cout<<"Opción:  ";
				getline(cin, in);
				is_true = this->validar_int(in);
				if(is_true == true){
					temp = stoi(in);
					if(temp < 0 || temp > 7){
						cout<< "opción no valida, intente nuevamente"<<endl;
					}else{
						cond = 1;
					}
				}else{
					cout<<"Ingreso no válido, intente nuevamente"<<endl;
				}				
				
			}
			return temp;				
		}		
		
		
};
