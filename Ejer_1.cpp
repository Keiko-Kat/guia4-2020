#include "Nodo.h"
#include "Arbol.h"
#include "Ejer_1.h"
#include <string>
#include <fstream>
#include <iostream>
using namespace std;

bool encuentra(Nodo *raiz, int num){
	int turn = 0;
	bool perhaps = false;
	
	if(raiz != NULL){
		if(num < raiz->numero){
			if(raiz->izq == NULL){
				cout<<"El numero que busca no se encuentra"<<endl;
			}else{
				perhaps = encuentra(raiz->izq,num);
			}
		}else{
			if(num > raiz->numero){
				if(raiz->der == NULL){
					cout<<"El numero que busca no se encuentra"<<endl;
				}else{
					perhaps = encuentra(raiz->der,num);
				}
			}else{
				perhaps = true;
			}
		}		
	}
	return perhaps;
}



void lectura_pre(Nodo *raiz){
	if(raiz != NULL){
		if(raiz->numero < 0){
			cout<<"-("<<raiz->numero<<")-";
		}else{
			cout<<"-"<<raiz->numero<<"-";
		}
		lectura_pre(raiz->izq);
		lectura_pre(raiz->der);
	}
}

void lectura_in(Nodo *raiz){
	if(raiz != NULL){
		lectura_in(raiz->izq);
		if(raiz->numero < 0){
			cout<<"-("<<raiz->numero<<")-";
		}else{
			cout<<"-"<<raiz->numero<<"-";
		}
		lectura_in(raiz->der);
	}
}

void lectura_pos(Nodo *raiz){
	if(raiz != NULL){
		lectura_pos(raiz->izq);
		lectura_pos(raiz->der);
		if(raiz->numero < 0){
			cout<<"-("<<raiz->numero<<")-";
		}else{
			cout<<"-"<<raiz->numero<<"-";
		}
	}
}





int main (void) {
    Ejercicio e = Ejercicio();
    Arbol *arbol = e.get_arbol();
    string in;
    bool is_true, perhaps;
    Nodo *nodo;
    int num, temp = 1;
    while(temp != 0){
		cout<<"Ingrese un numero entero:"<<endl;
		cout<<"   ~~~> ";
		getline(cin, in);
		is_true = e.validar_int(in);
		if(is_true == true){
			num = stoi(in);
			arbol->crear(num);
			temp = 0;
		}else{
			cout<<"Ingreso no válido"<<endl;
			cout<<"Intente nuevamente"<<endl;
		}
	}
    temp = e.menu();
    
    Nodo *raiz = arbol->get_raiz();
    
    temp = 1;
    while(temp != 0){
		
		switch(temp){
			case 1: cout<<"Ingrese un numero entero:"<<endl;
					cout<<"   ~~~> ";
					getline(cin, in);
					is_true = e.validar_int(in);
					if(is_true == true){
						num = stoi(in);
						arbol->insertar(num, raiz);
					}else{
						cout<<"Ingreso no válido"<<endl;
					}
					
					break;
					
			case 2: cout<<"Ingrese el numero entero a eliminar:"<<endl;
					cout<<"   ~~~> ";
					getline(cin, in);
					is_true = e.validar_int(in);
					if(is_true == true){
						num = stoi(in);
						arbol->eliminar(num, raiz);
						cout<<"El numero "<<num<<" fue eliminado"<<endl;
					}else{
						cout<<"Ingreso no válido"<<endl;
					}
					break;		
			case 3: cout<<"Ingrese el numero entero a modificar:"<<endl;
					cout<<"   ~~~> ";
					getline(cin, in);
					is_true = e.validar_int(in);
					if(is_true == true){
						num = stoi(in);
						perhaps = encuentra(raiz, num);
						if(perhaps == true){						
							cout<<"Ingrese el numero entero que lo reemplazara:"<<endl;
							cout<<"   ~~~> ";
							getline(cin, in);
							is_true = e.validar_int(in);
							if(is_true == true){
								arbol->eliminar(num, raiz);
								cout<<"El numero "<<num<<" ";
								num = stoi(in);
								cout<<" fue reemplazado por "<<num<<endl;
								arbol->insertar(num, raiz);
								
							}else{
								cout<<"Ingreso no válido"<<endl;
							}
						}
					}else{
						cout<<"Ingreso no válido"<<endl;
					}
					break;
			
			
			case 4: lectura_pre(raiz);
					cout<<endl;
					break;
			
			case 5: lectura_in(raiz);
					cout<<endl;
					break;
			
			case 6: lectura_pos(raiz);
					cout<<endl;
					break;
			
			case 7:	arbol->crearGrafo();
				break;
			
			default: break;
		}	
						
		temp = e.menu();
		
		
	}
		
    return 0;
}
