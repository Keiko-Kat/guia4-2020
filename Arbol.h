#include <iostream>
#include <string>
#include <fstream>
#include "Nodo.h"
using namespace std;

#ifndef ARBOL_H
#define ARBOL_H

class Arbol {
    public:
        Nodo *raiz = NULL;
        /* constructor*/
        Arbol();
        /* crea un nuevo nodo, recibe una instancia de numeros. */
        void crear (int numero);
        Nodo *crear_nodo(int numero);
        
        Nodo *get_raiz();
        
        void insertar(int num, Nodo *raiz);
        void eliminar(int num, Nodo *raiz);
        
        void recorrerArbol(Nodo *p, ofstream &);
        void crearGrafo();
};
#endif
